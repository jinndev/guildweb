import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class DeviceDetectorService {
  constructor() {}

  isMobile(): boolean {
    const mobileWidthBreakpoint = 768;
    return window.innerWidth <= mobileWidthBreakpoint;
  }
}
