import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

export interface User {
  firstName: string;
  lastName: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private user: User | null;
  private userSubject: BehaviorSubject<User | null>;

  constructor() {
    this.loadUserFromLocalStorage();
    this.userSubject = new BehaviorSubject<User | null>(this.user);
  }

  public isLoggedIn(): boolean {
    return this.user !== null;
  }

  public getUser(): User | null {
    return this.user;
  }

  public getUserObservable(): Observable<User | null> {
    return this.userSubject.asObservable();
  }

  public setUser(user: User): void {
    this.user = user;
    this.userSubject.next(this.user);
    this.saveUserToLocalStorage();
  }

  public clearUser(): void {
    this.user = null;
    this.userSubject.next(this.user);
    this.saveUserToLocalStorage();
  }

  private loadUserFromLocalStorage(): void {
    const userJson = localStorage.getItem('user');
    if (userJson) {
      this.user = JSON.parse(userJson);
    } else {
      this.user = null;
    }
  }

  private saveUserToLocalStorage(): void {
    if (this.user) {
      localStorage.setItem('user', JSON.stringify(this.user));
    } else {
      localStorage.removeItem('user');
    }
  }
}
