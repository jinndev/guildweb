import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './components/about/about.component';
import { HomeComponent } from './components/home/home.component';
import { MembershipComponent } from './components/membership/membership.component';
import { NewsComponent } from './components/news/news.component';
import { JinnComponent } from './components/jinn/jinn.component';
import { RanksComponent } from './components/ranks/ranks.component';
import { ClientsComponent } from './components/clients/clients.component';
import { AdvocacyComponent } from './components/advocacy/advocacy.component';
import { DisciplinesComponent } from './components/disciplines/disciplines.component';
import { GlossaryComponent } from './components/glossary/glossary.component';
import { ContactUsComponent } from './components/contactus/contactus.component';
import { LoginComponent } from './components/login/login.component';
import { ConductComponent } from './components/conduct/conduct.component';

const routes: Routes = [
    { path: 'about', component: AboutComponent },
    { path: 'advocacy', component: AdvocacyComponent },
    { path: 'disciplines', component: DisciplinesComponent },
    { path: 'glossary', component: GlossaryComponent },
    { path: 'membership', component: MembershipComponent },
    { path: 'news', component: NewsComponent },
    { path: 'jinn', component: JinnComponent },
    { path: 'ranks', component: RanksComponent },
    { path: 'clients', component: ClientsComponent },
    { path: 'contactus', component: ContactUsComponent },
    { path: 'login', component: LoginComponent },
    { path: 'conduct', component: ConductComponent },
    { path: '**', component: HomeComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
