export class MenuItem {
  id: string;
  title: string;
  href?: string;
  icon?: string;
  route?: string;
  submenu?: MenuItem[];
  onClick?: () => unknown;
}
