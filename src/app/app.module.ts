import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatTreeModule } from '@angular/material/tree';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatStepperModule } from '@angular/material/stepper';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatDividerModule } from '@angular/material/divider';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatCardModule } from '@angular/material/card';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { IsVisibleDirective } from './directives/is-visible.directive';
import { AboutComponent } from './components/about/about.component';
import { MembershipComponent } from './components/membership/membership.component';
import { JinnComponent } from './components/jinn/jinn.component';
import { NewsComponent } from './components/news/news.component';
import { RanksComponent } from './components/ranks/ranks.component';
import { ClientsComponent } from './components/clients/clients.component';
import { HeaderMenuComponent } from './components/header/menu/header-menu.component';
import { SidenavMenuComponent } from './components/sidenav/menu/sidenav-menu.component';
import { AdvocacyComponent } from './components/advocacy/advocacy.component';
import { DisciplinesComponent } from './components/disciplines/disciplines.component';
import { GlossaryComponent } from './components/glossary/glossary.component';
import { ContactUsComponent } from './components/contactus/contactus.component';
import { LoginComponent } from './components/login/login.component';
import { ConductComponent } from './components/conduct/conduct.component';
import { GuildLogoComponent } from './components/guild-logo/guild-logo.component';
import { WizardComponent } from './components/signup-wizard/signup-wizard.component';
import { SignupWizardBottomSheetComponent } from './components/signup-wizard/bottom-sheet/signup-wizard-bottom-sheet.component';
import { SignupWizardModalComponent } from './components/signup-wizard/modal/signup-wizard-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AdvocacyComponent,
    GlossaryComponent,
    DisciplinesComponent,
    HeaderComponent,
    FooterComponent,
    SidenavComponent,
    IsVisibleDirective,
    AboutComponent,
    MembershipComponent,
    JinnComponent,
    RanksComponent,
    ClientsComponent,
    NewsComponent,
    ContactUsComponent,
    LoginComponent,
    ConductComponent,
    HeaderMenuComponent,
    SidenavMenuComponent,
    GuildLogoComponent,
    WizardComponent,
    SignupWizardBottomSheetComponent,
    SignupWizardModalComponent
  ],
  imports: [
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    MatToolbarModule,
    MatButtonModule,
    MatMenuModule,
    MatSidenavModule,
    MatIconModule,
    MatTreeModule,
    MatBottomSheetModule,
    MatStepperModule,
    MatFormFieldModule,
    MatDialogModule,
    MatInputModule,
    MatDividerModule,
    FormsModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatCardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
