import { AfterViewInit, Directive, ElementRef, EventEmitter, Input, Output, TemplateRef, ViewContainerRef } from '@angular/core'

@Directive({
    selector: '[isVisible]',
})
export class IsVisibleDirective implements AfterViewInit {

    @Input()
    viewportMargin: string = "-100px";

    @Output()
    visibilityChange: EventEmitter<boolean> = new EventEmitter(false);

    constructor(private elementRef: ElementRef<any>) { }

    ngAfterViewInit() {
        const observedElement = this.elementRef.nativeElement;

        const observer = new IntersectionObserver(([entry]) => {
            if (entry.isIntersecting) {
                console.log('is Visible');
                this.visibilityChange.emit(true);
            }
            else {
                console.log('is NOT Visible');
                this.visibilityChange.emit(false);
            }
        }, { 
            rootMargin: this.viewportMargin
        });

        observer.observe(observedElement)
    }
}