import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

    constructor(private iconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer) {
        this.iconRegistry.addSvgIcon(
            'facebook', this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/icons/facebook.svg')
        ).addSvgIcon(
            'twitter', this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/icons/twitter.svg')
        ).addSvgIcon(
            'instagram', this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/icons/instagram.svg')
        ).addSvgIcon(
            'linkedin', this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/icons/linkedin.svg')
        ).addSvgIcon(
            'youtube', this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/icons/youtube.svg')
        )
    }

    ngOnInit(): void {
    }

}
