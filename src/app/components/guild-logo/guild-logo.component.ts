import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-guild-logo',
    templateUrl: './guild-logo.component.html',
    styleUrls: ['./guild-logo.component.scss'],
    encapsulation: ViewEncapsulation.ShadowDom
})
export class GuildLogoComponent {

    @Input()
    logo: 'owl' | 'owlLogoHorizontal' | 'owlLogoVertical' | 'guildOwl';

    @Input()
    class: string;

    constructor() { }
}
