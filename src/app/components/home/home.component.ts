import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { SignupWizardService } from '../signup-wizard/signup-wizard.service';
import { Subscription } from 'rxjs';
import { User, AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  private userSub: Subscription;
  user: User | null;

  constructor(
    private signupWizardService: SignupWizardService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.userSub = this.authService.getUserObservable().subscribe((user) => {
      this.user = user;
    });
  }

  ngOnDestroy() {
    if (this.userSub) {
      this.userSub.unsubscribe();
    }
  }

  visibilityChanged(event: boolean) {
    console.log(event);
  }

  onJoinClick() {
    this.signupWizardService.openWizard();
  }
}
