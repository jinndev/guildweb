import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JinnComponent } from './jinn.component';

describe('JinnComponent', () => {
  let component: JinnComponent;
  let fixture: ComponentFixture<JinnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JinnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JinnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
