import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-disciplines',
  templateUrl: './disciplines.component.html',
  styleUrls: ['./disciplines.component.scss']
})
export class DisciplinesComponent implements OnInit {

  disciplines = [
    { Title: "Software Engineering", Examples: "Software Architecture, Engineering, Development, and SDET", 
        Description: "Software Engineering goes beyond the act of writing code, but of engineering software.  This extends far beyond the symbols and syntax written into an IDE.  Planning the code, following principles and industry standards, proper documentation via Testing, API Documentation, Graphs, and far more.  Engineering Principles are the way of the Software Engineer.  Software Engineering includes Coding and Programming, but not all Coding or Programming is Software Engineering." },
    { Title: "IT Architecture", Examples: "Site Reliability Engineering, Systems Architecture, Infrastructure, Operations, and DevOps",
        Description: "Focuses on system reliability, infrastructure, and development process efficiency. DevOps aims to empower collaboration and equip the SDLC process with automation of frequent, gradual changes. SRE aims to ensure availability and reliability of operating the system at scale using change management practices and service level commitments. These groups heavily participate in the organization's struggle against Conway's Law." },
    { Title: "Information Security", Examples: "IT Security, CyberSecurity, InfoSec",
        Description: "The most secure system is the most useless system.  Gold locked in a chest is worthless.  In order to get to technical data, whether it is in a database, on a website, or having been printed or spoken between two people, we need to expose that data outside of it's secure location.  Information Security is not the process of securely locking up information, it is the process of exposing it securely." },
    { Title: "Data Architecture", Examples: "Data Architecture and Database Administration",
        Description: "In the deepest fathoms of a system lies its persistence.  The place where we store tweets, social security numbers, and descriptions of our pets.  The place where quadrillions of data transfer units move in and out every nanosecond!!!  Those who work within the Data Architecture discipline ensure we are creating schemas and structures with the best practices, ensure our queries have the highest performance, our data is secure and backed up, and we have disaster recovery." },
    { Title: "Technical Support", Examples: "Desktop Support and Customer Support",
        Description: "After all of the Vision, Technical Work, Sales and Marketing, and Release of some technical item, it needs to be supported.  Technical Support staff read and master the technical documentation of products, and provide an interface between consumers and the technical team.  Creating bugs, enhancement requests, working with consumers to understand the software then communicating the need for better documentation to the Technical Writers.  An amazing Technical Support specialist has to see both sides of the coin and have the communication and soft skills necessary to ensure customer retention." },
    { Title: "User Experience", Examples: "User Experience and UX Design",
        Description: "Though Van Gogh created amazing art, and Ron Swanson builds amazing furniture, you need an interior designer to make a living room work with that art.  A UX professional can pull together the technical requirements such as form fields, buttons, information dissemination, and more, and can enter into a constant push and pull with the consumers to determine layouts and details that encourage the best reception, click through, persistence, and other quality factors that enhance the user's experience far beyond great media design alone." },
    { Title: "Quality Engineering", Examples: "Quality Assurance and Business Analysis",
        Description: "Shift Left!!  Quality Engineering requires a similar understanding of system behavior, if not at sliced up scopes, as Product Owners.  QA Analysts write test cases for small component testing, end to end tests, and regression tests.  Quality Engineering is the act of knowing where and how to test, and how to work together with the development team with a tight feedback loop." },
    { Title: "Team Engineering", Examples: "Business Analysts, Product Owners, Agile Leaders, Project Managers, etc",
        Description: "The SDLC involves project styles from the most novice worker up to the company owner.  Pulling the team together into an organized group takes a number of roles working well in sync.  Analysts translate between the Tech Team and other business members and control the content of Stories, Product Owners are SMEs of the behavior of the systems who create Stories and Epics, Project Managers group initiatives together and associate them with the team's work to translate Effort and Complexity into value for the board." },
    { Title: "Media Design", Examples: "Media Design and Editing (UI, Logos, Art, Audio, Video Editing, etc)",
        Description: "In the scope of Information Technology, Media involves logos, banners, 3d modelling, video editing, audio editing, and other forms of media creation specifically for technology such as websites, apps, and video games." },
    { Title: "Technical Writing", Examples: "Technical Writers and LMS coordinators",
        Description: "As feedback from end users, internal users, the business and vision teams, and other requirements roll in, they become obscured by the technical tasks and work performed to bring them to life.  Technical Writing is any work which takes all of these disperate sources, and translates the technical releases into descriptions understandable to consumers.  This could be technical Release Notes, Blogging about Technology, FAQ or technical documentation, Press Releases, LMS and Wizard creations, or overall website content related to technology." },
    { Title: "IT-Adjacent Professionals", Examples: "HR professional, IT Manager, CTO, CIO, etc",
        Description: "Any professional for whom the most major portion of their work is to support the prior disciplines, but who might not have their hands directly on the work.  Such as an IT HR professional, an IT Manager, CTO, CIO, et cetera." }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
