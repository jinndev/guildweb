import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription, takeWhile } from 'rxjs';
import { MenuItem } from 'src/app/models/menu-item.model';
import { User, AuthService } from 'src/app/services/auth.service';
import { SignupWizardService } from '../signup-wizard/signup-wizard.service';
import { PRIMARY_MENU_ITEMS, TOP_MENU_ITEMS } from 'src/app/constants/menus';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit, OnDestroy {
  private userSub: Subscription;
  user: User | null;
  topMenu: MenuItem[];
  primaryMenu: MenuItem[];

  constructor(
    private signupWizardService: SignupWizardService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.userSub = this.authService.getUserObservable().subscribe((user) => {
      this.user = user;
    });
    const updatedMenuItems = TOP_MENU_ITEMS.map((m) => {
      if (m.id !== 'signup') {
        return m;
      }
      return {
        ...m,
        onClick: this.onJoinClick.bind(this)
      };
    });
    this.topMenu = updatedMenuItems;
    this.primaryMenu = PRIMARY_MENU_ITEMS;
  }

  ngOnDestroy() {
    if (this.userSub) {
      this.userSub.unsubscribe();
    }
  }

  onJoinClick() {
    this.signupWizardService.openWizard();
  }

  signOut() {
    this.authService.clearUser();
  }
}
