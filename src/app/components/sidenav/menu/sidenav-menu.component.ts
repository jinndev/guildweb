import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, Input, OnInit } from '@angular/core';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { MenuItem } from 'src/app/models/menu-item.model';

@Component({
  selector: 'app-sidenav-menu',
  templateUrl: './sidenav-menu.component.html',
  styleUrls: ['./sidenav-menu.component.scss']
})
export class SidenavMenuComponent {
  @Input()
  set menuItems(value: MenuItem[]) {
    this.dataSource.data = value;
  }

  treeControl = new NestedTreeControl<MenuItem>((x) => x.submenu);
  dataSource = new MatTreeNestedDataSource<MenuItem>();

  constructor() {}

  hasChild = (_: number, node: MenuItem) =>
    !!node.submenu && node.submenu.length > 0;

  handleMenuItemClick(menuItem: MenuItem) {
    if (!menuItem.onClick) {
      return;
    }
    menuItem.onClick();
  }
}
