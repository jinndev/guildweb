import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
  FormArray
} from '@angular/forms';
import { SignupWizardService } from './signup-wizard.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-signup-wizard',
  templateUrl: './signup-wizard.component.html',
  styleUrls: ['./signup-wizard.component.scss']
})
export class WizardComponent implements OnInit {
  submitting = false;
  successfulSubmission = false;
  errorSubmission = false;
  contactForm: FormGroup;
  interestsForm: FormGroup;
  socialMediaForm: FormGroup;
  sourceForm: FormGroup;
  wizardForm: FormArray;
  readCodeOfConduct: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private signupWizardService: SignupWizardService,
    private authService: AuthService
  ) {}

  get completedForm() {
    return this.successfulSubmission || this.errorSubmission;
  }

  codeOfConductControl = new FormControl(false);
  interests = new FormControl();

  interestList = [
    'Software Engineering',
    'Platform Engineering',
    'Data Engineering',
    'Quality Engineering',
    'Information Security',
    'User and Tech Support',
    'User Experience and Design',
    'Media Design',
    'Product Ownership',
    'Technical Writing',
    'Other Supporting Roles'
  ];

  placeList = ['Facebook', 'Instagram', 'LinkedIn', 'YouTube', 'Other'];

  selectedInterests = [];
  selectedPlace = [];
  selectedValue: string = '';

  ngOnInit(): void {
    this.initForms();
  }

  initForms() {
    this.contactForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      emailAddress: [
        null,
        Validators.compose([
          Validators.pattern(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          )
        ])
      ],
      phoneNumber: [
        null,
        Validators.compose([
          Validators.pattern(
            /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im
          )
        ])
      ]
    });

    this.interestsForm = this.formBuilder.group({
      interests: this.interests
    });

    this.socialMediaForm = this.formBuilder.group({
      linkedInProfile: [''],
      githubProfile: ['']
    });

    this.sourceForm = this.formBuilder.group(
      {
        source: [''],
        sourceOther: ['']
      },
      { validators: this.otherRequired }
    );

    this.wizardForm = new FormArray([
      this.contactForm,
      this.interestsForm,
      this.socialMediaForm,
      this.sourceForm
    ]);
  }

  otherRequired(formGroup: FormGroup) {
    const source = formGroup.get('source');
    const sourceOther = formGroup.get('sourceOther');

    if (
      source?.value === 'Other' &&
      (!sourceOther?.value || sourceOther?.value.trim() === '')
    ) {
      return { otherRequired: true };
    }
    return null;
  }

  join() {
    const contactFormData = this.contactForm.value;
    const interestsFormData = this.interestsForm.value;
    const socialMediaFormData = this.socialMediaForm.value;
    const sourceFormData = this.sourceForm.value;

    this.submitting = true;
    this.successfulSubmission = false;
    this.errorSubmission = false;
    this.signupWizardService
      .sendSignupNotificationEmail({
        firstName: contactFormData.firstName,
        lastName: contactFormData.lastName,
        emailAddress: contactFormData.emailAddress,
        phoneNumber: contactFormData.phoneNumber,
        interests: interestsFormData.interests,
        linkedInProfile: socialMediaFormData.linkedInProfile,
        githubProfile: socialMediaFormData.githubProfile,
        source: sourceFormData.source,
        sourceOther: sourceFormData.sourceOther
      })
      .subscribe({
        next: () => {
          this.submitting = false;
          this.authService.setUser({
            firstName: contactFormData.firstName,
            lastName: contactFormData.lastName
          });
          this.successfulSubmission = true;
        },
        error: () => {
          this.submitting = false;
          this.errorSubmission = true;
        }
      });
  }
}
