import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-signup-wizard-modal',
  templateUrl: './signup-wizard-modal.component.html',
  styleUrls: ['./signup-wizard-modal.component.scss']
})
export class SignupWizardModalComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<SignupWizardModalComponent>) {}

  ngOnInit(): void {}

  closeModal() {
    this.dialogRef.close();
  }
}
