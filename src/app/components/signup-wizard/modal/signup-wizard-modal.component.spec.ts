import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupWizardModalComponent } from './signup-wizard-modal.component';

describe('ModalComponent', () => {
  let component: SignupWizardModalComponent;
  let fixture: ComponentFixture<SignupWizardModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SignupWizardModalComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupWizardModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
