import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import template from 'lodash/template';
import {
  MatBottomSheet,
  MatBottomSheetRef
} from '@angular/material/bottom-sheet';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { SignupWizardBottomSheetComponent } from './bottom-sheet/signup-wizard-bottom-sheet.component';
import { SignupWizardModalComponent } from './modal/signup-wizard-modal.component';
import { DeviceDetectorService } from 'src/app/services/device-detector.service';

export interface SignupData {
  firstName: string;
  lastName: string;
  emailAddress: string;
  phoneNumber?: string;
  interests?: string[];
  linkedInProfile?: string;
  githubProfile?: string;
  source: string;
  sourceOther?: string;
}

@Injectable({
  providedIn: 'root'
})
export class SignupWizardService {
  bottomSheetRef: MatBottomSheetRef<SignupWizardBottomSheetComponent, any>;
  dialogRef: MatDialogRef<SignupWizardModalComponent, any>;

  constructor(
    private http: HttpClient,
    private matBottomSheet: MatBottomSheet,
    private dialog: MatDialog,
    private deviceDetectorService: DeviceDetectorService
  ) {}

  private createSignupSummaryText(data: SignupData): string {
    const notEntered = 'Not entered by user';

    const compiled = template(/*html*/ `
      <p>A new user has signed up for the Guild. Please see information below:</p>
      <ul>
        <li>First Name: <%= firstName ? firstName : notEntered %></li>
        <li>Last Name: <%= lastName ? lastName : notEntered %></li>
        <li>Email Address: <%= emailAddress ? emailAddress : notEntered %></li>
        <li>Phone Number: <%= phoneNumber ? phoneNumber : notEntered %></li>
        <li>Interests: <%= interests ? interests.join(', ') : notEntered %></li>
        <li>LinkedIn Profile: <%= linkedInProfile ? linkedInProfile : notEntered %></li>
        <li>GitHub Profile: <%= githubProfile ? githubProfile : notEntered %></li>
        <li>Source: <%= source ? source : notEntered %></li>
        <li>Source Other: <%= sourceOther ? sourceOther : notEntered %></li>
      </ul>
      <p>Data in JSON format:</p>
      <code>
        <%= JSON.stringify(data, null, 2) %>
      </code>
    `);

    return compiled({ ...data, notEntered, data });
  }

  sendSignupNotificationEmail(data: SignupData): Observable<void> {
    const body = this.createSignupSummaryText(data);
    const url = 'https://jinnguildapi.azurewebsites.net/api/Email/Contact';
    return this.http.post<void>(url, {
      body
    });
  }

  openWizard() {
    if (this.deviceDetectorService.isMobile()) {
      this.bottomSheetRef = this.matBottomSheet.open(
        SignupWizardBottomSheetComponent,
        {
          panelClass: 'joinBottomSheet',
          disableClose: true,
          hasBackdrop: true
        }
      );
      return;
    }
    this.dialogRef = this.dialog.open(SignupWizardModalComponent, {
      panelClass: 'joinModal',
      disableClose: true,
      hasBackdrop: true
    });
  }

  closeWizard() {
    if (this.bottomSheetRef) {
      this.bottomSheetRef.dismiss();
    }
    if (this.dialogRef) {
      this.dialogRef.close();
    }
  }
}
