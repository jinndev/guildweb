import { Component, OnInit } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-signup-wizard-bottom-sheet',
  templateUrl: './signup-wizard-bottom-sheet.component.html',
  styleUrls: ['./signup-wizard-bottom-sheet.component.scss']
})
export class SignupWizardBottomSheetComponent implements OnInit {
  constructor(
    public bottomSheetRef: MatBottomSheetRef<SignupWizardBottomSheetComponent>
  ) {}

  ngOnInit(): void {}

  closeBottomSheet() {
    this.bottomSheetRef.dismiss();
  }
}
