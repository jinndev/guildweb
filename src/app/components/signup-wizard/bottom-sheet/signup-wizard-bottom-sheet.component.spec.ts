import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupWizardBottomSheetComponent } from './signup-wizard-bottom-sheet.component';

describe('BottomSheetComponent', () => {
  let component: SignupWizardBottomSheetComponent;
  let fixture: ComponentFixture<SignupWizardBottomSheetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SignupWizardBottomSheetComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupWizardBottomSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
