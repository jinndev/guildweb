import {
  Component,
  Input,
  OnInit,
  QueryList,
  ViewChildren
} from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { MenuItem } from 'src/app/models/menu-item.model';

@Component({
  selector: 'app-header-menu',
  templateUrl: './header-menu.component.html',
  styleUrls: ['./header-menu.component.scss']
})
export class HeaderMenuComponent implements OnInit {
  @Input()
  menuItem: MenuItem;

  @Input()
  color: 'primary';

  @ViewChildren('menuTrigger')
  menuTriggers: QueryList<MatMenuTrigger>;

  constructor() {}

  ngOnInit(): void {}

  handleMenuItemClick(menuItem: MenuItem) {
    if (!menuItem.onClick) {
      return;
    }
    menuItem.onClick();
  }
}
