import {
  animate,
  state,
  style,
  transition,
  trigger
} from '@angular/animations';
import {
  Component,
  EventEmitter,
  HostListener,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import { Subscription, takeWhile } from 'rxjs';
import { MenuItem } from 'src/app/models/menu-item.model';
import { AuthService, User } from 'src/app/services/auth.service';
import { SignupWizardService } from '../signup-wizard/signup-wizard.service';
import { PRIMARY_MENU_ITEMS, TOP_MENU_ITEMS } from 'src/app/constants/menus';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [
    trigger('hideShow', [
      state(
        'hidden',
        style({
          transform: 'translateY(-100%)'
        })
      ),
      transition('* <=> *', [animate('.3s')])
    ])
  ]
})
export class HeaderComponent implements OnInit, OnDestroy {
  private userSub: Subscription;
  user: User | null;
  topMenu: MenuItem[];
  primaryMenu: MenuItem[];

  primaryMenuState = 'visible';

  private _scrollTop: number;

  @Output() public sidenavToggle = new EventEmitter();

  @HostListener('window: scroll')
  onWindowScroll() {
    if (window.scrollY >= 60 && window.scrollY > this._scrollTop) {
      this.primaryMenuState = 'hidden';
    } else {
      this.primaryMenuState = 'visible';
    }

    this._scrollTop = window.scrollY;
  }

  constructor(
    private authService: AuthService,
    private signupWizardService: SignupWizardService
  ) {}

  ngOnInit(): void {
    this.userSub = this.authService.getUserObservable().subscribe((user) => {
      this.user = user;
    });
    const updatedMenuItems = TOP_MENU_ITEMS.map((m) => {
      if (m.id !== 'signup') {
        return m;
      }
      return {
        ...m,
        onClick: this.onJoinClick.bind(this)
      };
    });
    this.topMenu = updatedMenuItems;
    this.primaryMenu = PRIMARY_MENU_ITEMS;
  }

  ngOnDestroy() {
    if (this.userSub) {
      this.userSub.unsubscribe();
    }
  }

  signOut() {
    this.authService.clearUser();
  }

  onToggleSidenav() {
    this.sidenavToggle.emit();
  }

  onJoinClick() {
    this.signupWizardService.openWizard();
  }
}
