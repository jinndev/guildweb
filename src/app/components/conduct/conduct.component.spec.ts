import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConductComponent } from './conduct.component';

describe('AdvocacyComponent', () => {
    let component: ConductComponent;
    let fixture: ComponentFixture<ConductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
        declarations: [ConductComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
      fixture = TestBed.createComponent(ConductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
