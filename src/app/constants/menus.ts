import { MenuItem } from '../models/menu-item.model';

export const TOP_MENU_ITEMS: MenuItem[] = [{ id: 'signup', title: 'Join' }];

export const PRIMARY_MENU_ITEMS: MenuItem[] = [
  { id: 'about_us', title: 'About Us', route: 'about' },
  { id: 'membership', title: 'Membership', route: 'membership' },
  { id: 'clients', title: 'Clients', route: 'clients' },
  { id: 'news', title: 'News', route: 'news' },
  {
    id: 'advocacy_initiatives',
    title: 'Advocacy & Initiatives',
    route: 'advocacy'
  },
  {
    id: 'resources',
    title: 'Resources',
    submenu: [
      {
        id: 'membership_agreement',
        title: 'Membership Agreement',
        href: 'assets/agreement.pdf'
      },
      {
        id: 'agreement_standards',
        title: 'Agreement Standards',
        href: 'assets/standards.pdf'
      },
      {
        id: 'guild_mediation',
        title: 'Guild Mediation',
        href: 'assets/nonsolicitation.pdf'
      },
      {
        id: 'agreement_definitions',
        title: 'Agreement Definitions',
        href: 'assets/definitions.pdf'
      }
    ]
  },
  { id: 'disciplines', title: 'Disciplines', route: 'disciplines' }
];
